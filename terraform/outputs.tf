output "pet_name" {
  value = random_pet.main.id
}

output "url" {
  value = aws_s3_bucket_website_configuration.main.website_endpoint
}
