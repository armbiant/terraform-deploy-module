<html>
  <head>
    <title>${pet}</title>
    <style>
    body {
      background-color: #a28089;
      color: #ffffff;
    }
    
    div {
      position: fixed;
      top: 50%;
      left: 50%;
      transform: translate(-50%, -50%);
      font-size: 5vw;
    }
    </style>
  </head>
  <body>
    <div>Hello ${pet}!</div>
  <body>
</html>