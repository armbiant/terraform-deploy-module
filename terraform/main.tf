locals {
  # NOTE: define AWS default tags here, because it's meant to be used as a Terraform module
  #       which must define the `provider` block.
  aws_default_tags = {
    Name        = var.name
    Environment = var.environment
    Project     = random_pet.main.id
  }
}

resource "random_pet" "main" {
  length = var.pet_name_length
}

resource "aws_s3_bucket" "main" {
  bucket = "${var.environment}.${var.name}"

  tags = local.aws_default_tags
}

resource "aws_s3_bucket_website_configuration" "main" {
  bucket = aws_s3_bucket.main.id

  index_document {
    suffix = "index.html"
  }
}

resource "aws_s3_bucket_acl" "main" {
  bucket = aws_s3_bucket.main.id

  acl = "public-read"
}

resource "aws_s3_bucket_policy" "main" {
  bucket = aws_s3_bucket.main.id

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Sid       = "PublicReadGetObject"
        Effect    = "Allow"
        Principal = "*"
        Action    = "s3:GetObject"
        Resource = [
          aws_s3_bucket.main.arn,
          "${aws_s3_bucket.main.arn}/*",
        ]
      },
    ]
  })
}

locals {
  index_content = templatefile("${path.module}/templates/index.html.tpl", { pet = random_pet.main.id })
}

resource "aws_s3_object" "main" {
  bucket       = aws_s3_bucket.main.id
  key          = "index.html"
  content      = local.index_content
  content_type = "text/html"
  etag         = md5(local.index_content)

  tags = local.aws_default_tags
}
